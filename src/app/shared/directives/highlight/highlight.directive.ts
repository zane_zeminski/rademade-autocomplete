import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  @Input('appHighlight') searchTerm: string;

  constructor(private element: ElementRef) {
    setTimeout(() => {
        let el = this.element.nativeElement;
        let idx = el.innerText.toLowerCase().indexOf(this.searchTerm.toLowerCase());
        let textStart = el.innerText.substring(0, idx);
        let origMatchedText = el.innerText.substring(idx, idx + this.searchTerm.length);
        let textEnd = el.innerText.substring(idx + this.searchTerm.length);
        el.innerHTML = `${textStart}<span class="highlighted">${origMatchedText}</span>${textEnd}`;
        let highlighted = this.element.nativeElement.querySelector('.highlighted');
        highlighted.style.backgroundColor = 'yellow';
        highlighted.style.color = '#000';
    }, 0);
  }
}
