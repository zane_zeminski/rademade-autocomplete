import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { HostListener } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent {
  dropdownVisible:boolean = false;
  searchTermChanged: Subject<string> = new Subject<string>();
  selectedElementIdx: number = -1;
  matchedItems: any[] = [];
  searchTerm: string;
  private documentClickSubscription: Subscription;
  private dropdownElements = [];
  private isConfirmed = false;
  @Input('src') source: any[] = this.source || [];
  @Output() onSelect: EventEmitter<any> = new EventEmitter();
  @Output() onAutocomplete: EventEmitter<any> = new EventEmitter();
  @ViewChild('autocomplete__dropdown') dropdown: ElementRef;

  constructor(private element: ElementRef) {
    this.searchTermChanged
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(value => this.filter(value));
  }

  onKeyDown(event): void {
    switch (event.keyCode) {
      case 27:
        this.hideDropdown();
        break;
      case 13:
        let matchedItem = this.matchedItems[this.selectedElementIdx];
        this.confirmSelected(matchedItem.sourceIdx);
        break;
      case 38:
        this.selectElement(-1);
        event.preventDefault();
        break;
      case 40:
        this.selectElement(1);
        event.preventDefault();
        break;
    }
  }

  elementHover(idx): void {
    this.selectedElementIdx = idx;
  }

  private confirmSelected(sourceIdx): void {
    this.onSelect.emit(sourceIdx);
    this.searchTerm = '';
    this.isConfirmed = true;
    this.hideDropdown();
  }

  private getDropdownElements(): any[] {
    if (!this.dropdown) return;
    let dropdown = this.dropdown.nativeElement;
    return dropdown.querySelectorAll('.autocomplete__dropdown-el');
  }

  private selectElement(dir): void {
    this.selectedElementIdx += dir;
    if (this.selectedElementIdx > this.dropdownElements.length - 1) {
      this.selectedElementIdx = 0;
    } else if (this.selectedElementIdx < 0) {
      this.selectedElementIdx = this.dropdownElements.length - 1;
    }
  }

  private hideDropdown(): void {
    this.dropdownVisible = false;
    setTimeout(() => {
      if (this.documentClickSubscription)
        this.documentClickSubscription.unsubscribe();
    }, 0);
  }

  private showDropdown(): void {
    this.dropdownVisible = true;
    if (this.isConfirmed) {
      this.onAutocomplete.emit();
      this.isConfirmed = false;
    };

    setTimeout(() => {
      this.dropdownElements = this.getDropdownElements();
      let documentClickObservable: Observable<MouseEvent> = Observable
        .fromEvent(document, 'click');
        this.documentClickSubscription = documentClickObservable
        .subscribe(event => {
          if (!this.element.nativeElement.contains(event.target)) {
            this.hideDropdown();
          }
        });
    }, 0);
  }

  private filter(searchTerm: string): void {
    if (searchTerm.length === 0) {
      this.hideDropdown();
      return;
    }
    this.matchedItems = [];
    let idx = -1;
    let matchedItem = {};
    this.source.forEach((item, index) => {
      if (typeof item === 'object') {
        Object.keys(item).forEach((key) => {
          idx = item[key].toLowerCase().indexOf(searchTerm.toLowerCase());
          matchedItem = { matchIdx: idx, text: item[key], sourceIdx: index };
          if (idx >= 0) {
            this.matchedItems.push(matchedItem)
          };
        });
      } else if (typeof item === 'string') {
        idx = item.toLowerCase().indexOf(searchTerm.toLowerCase());         
        matchedItem = { matchIdx: idx, text: item, sourceIdx: index };
        if (idx >= 0) {
          this.matchedItems.push(matchedItem)
        };
      }
    });
    this.matchedItems.sort((a, b) => a.matchIdx - b.matchIdx);
    this.matchedItems.splice(5, this.matchedItems.length - 1);
    if (this.matchedItems.length > 0) {
      this.selectedElementIdx = 0;
      this.showDropdown();
    } else {
      this.hideDropdown();
    }
  }

}
