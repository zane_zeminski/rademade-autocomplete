import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AutocompleteComponent } from './autocomplete.component';
import { HighlightModule } from '../../directives/highlight/highlight.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HighlightModule
  ],
  declarations: [AutocompleteComponent],
  exports: [AutocompleteComponent]
})
export class AutocompleteModule { }
