import { Component, OnInit } from '@angular/core';
import { ProductsService } from './shared/products.service';
import { Product } from './shared/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[];
  searchItems: any[]

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.getProducts();
  }

  onSelect(index: number): void {
    this.products = [this.products[index]];
  }

  getProducts(): void {
    this.productsService
      .getProducts()
      .subscribe(
        products => {
          this.products = products;
          this.searchItems =  [];
          this.products.forEach((product: Product) => {
            let searchItem = {
              name: product.name,
              description: product.description
            };
            this.searchItems.push(searchItem);
          });
        },
        error => console.log(error)
      );
  }
}
