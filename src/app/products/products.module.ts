import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { ProductsService } from './shared/products.service';
import { AutocompleteModule } from '../shared/components/autocomplete/autocomplete.module';

@NgModule({
  imports: [
    CommonModule,
    AutocompleteModule
  ],
  declarations: [
    ProductsComponent
  ],
  providers: [ProductsService],
})
export class ProductsModule { }
