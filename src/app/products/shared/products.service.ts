import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Product } from './product';

@Injectable()
export class ProductsService {

  constructor(private http: Http) { }

  getProducts(): Observable<Product[]> {
    return this.http
      .get('assets/products.json')
      .map((response: Response) => response.json());
  }
}
