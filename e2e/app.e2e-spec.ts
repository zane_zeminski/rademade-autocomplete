import { RademadePage } from './app.po';

describe('rademade App', () => {
  let page: RademadePage;

  beforeEach(() => {
    page = new RademadePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
